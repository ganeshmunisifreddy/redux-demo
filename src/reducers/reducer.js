export const songsReducer = () => {
	return [
		{title: "Love me like you do", duration: 4.25},
		{title: "Bad Blood", duration: 5.55},
		{title: "We don't talk anymore", duration: 6.50},
		{title: "Let me love you", duration: 3.22},
	]
};

export const selectedSongReducer = (selectedSong=null, action) => {
	if(action.type === "SONG_SELECTED"){
		return action.payload;
	}
	return selectedSong;
};