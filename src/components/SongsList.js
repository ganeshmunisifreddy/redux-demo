import React, {Component} from 'react';
import {connect} from 'react-redux';
import {selectSong} from "../actions";

class SongsList extends Component {

	state = {
		active: null
	};

	makeActive = (song, idx) => {
		this.setState({active: idx}, () => this.props.selectSong(song))
	};

	render() {
		const {songs} = this.props;
		return (
			<div className="list-group">
				{
					songs.map((song, idx) => (
						<button key={song.title} className={`list-group-item list-group-item-action ${this.state.active === idx ? 'active' : ""}`}
						        onClick={() => this.makeActive(song, idx)}>
							{song.title}
						</button>
					))
				}
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		songs: state.songs
	}
};

export default connect(mapStateToProps,{selectSong})(SongsList);