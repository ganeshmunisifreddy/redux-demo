import React, {Component} from 'react';
import SongDetails from "./SongDetails";
import SongsList from "./SongsList";

class App extends Component {
	render() {
		return (
			<div className="container">
				<div className="row" style={{marginTop: 100}}>
					<div className="col-md-6">
						<SongsList/>
					</div>
					<div className="col-md-6">
						<SongDetails/>
					</div>
				</div>
			</div>
		);
	}
}

export default App;