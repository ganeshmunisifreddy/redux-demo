import React, {Component} from 'react';
import {connect} from 'react-redux';

class SongDetails extends Component {

	detailRenderer = (song) => {
		if(song){
			return (
				<div className="card-body">
					<h5 className="card-title">
						Details:
					</h5>
					<div className="card-text">
						Song : {song.title}
						<br/>
						Duration: {song.duration}
					</div>
				</div>
			)
		} else {
			return (
				<div className="card-body">
					<h6 className="card-text">Select a song</h6>
				</div>
			);
		}
	};

	render() {
		const {song} = this.props;
		return (
			<div className="card">
				{this.detailRenderer(song)}
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		song: state.selectedSong
	}
};

export default connect(mapStateToProps)(SongDetails);